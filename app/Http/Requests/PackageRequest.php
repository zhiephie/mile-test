<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction' => 'required|array',
            'transaction.transaction_id' => 'required|uuid|unique:transactions',
            'transaction.customer_name' => 'required|max:255',
            'transaction.customer_code' => 'required',
            'transaction.transaction_amount' => 'required',
            'transaction.transaction_discount' => 'nullable',
            'transaction.transaction_additional_field' => 'nullable|array',
            'transaction.transaction_payment_type' => 'required|max:2',
            'transaction.transaction_state' => 'required',
            'transaction.transaction_code' => 'required',
            'transaction.transaction_order' => 'required|integer',
            'transaction.location_id' => 'required',
            'transaction.organization_id' => 'required|integer',
            'transaction.transaction_payment_type' => 'required',
            'transaction.transaction_cash_amount' => 'nullable|integer',
            'transaction.transaction_cash_change' => 'nullable|integer',
            'transaction.customer_attribute' => 'nullable|array',
            'connote' => 'required|array',
            'connote.connote_id' => 'required|uuid',
            'connote.connote_number' => 'required|integer',
            'connote.connote_service' => 'required',
            'connote.connote_service_price' => 'required|integer',
            'connote.connote_amount' => 'required|integer',
            'connote.connote_code' => 'required',
            'connote.connote_booking_code' => 'nullable',
            'connote.connote_order' => 'required|integer',
            'connote.connote_state' => 'required',
            'connote.connote_state_id' => 'required|integer',
            'connote.zone_code_from' => 'required',
            'connote.zone_code_to' => 'required',
            'connote.surcharge_amount' => 'nullable',
            'connote.actual_weight' => 'required|integer',
            'connote.volume_weight' => 'required|integer',
            'connote.chargeable_weight' => 'required|integer',
            'connote.organization_id' => 'required|integer',
            'connote.location_id' => 'required',
            'connote.connote_total_package' => 'required',
            'connote.connote_surcharge_amount' => 'required',
            'connote.connote_sla_day' => 'required',
            'connote.location_name' => 'required',
            'connote.location_type' => 'required',
            'connote.source_tariff_db' => 'required',
            'connote.id_source_tariff' => 'required',
            'connote.pod' => 'nullable',
            'connote.history' => 'nullable|array',
            'origin' => 'required|array',
            'origin.customer_name' => 'required',
            'origin.customer_address' => 'required',
            'origin.customer_email' => 'required',
            'origin.customer_phone' => 'required',
            'origin.customer_address_detail' => 'nullable',
            'origin.customer_zip_code' => 'required',
            'origin.zone_code' => 'required',
            'origin.organization_id' => 'required|integer',
            'origin.location_id' => 'required',
            'destination' => 'required|array',
            'destination.customer_name' => 'required',
            'destination.customer_address' => 'required',
            'destination.customer_email' => 'nullable',
            'destination.customer_phone' => 'required',
            'destination.customer_address_detail' => 'required',
            'destination.customer_zip_code' => 'required',
            'destination.zone_code' => 'required',
            'destination.organization_id' => 'required|integer',
            'destination.location_id' => 'required',
            'koli' => 'required|array',
            'koli.koli_length' => 'required|integer',
            'koli.awb_url' => 'required',
            'koli.koli_chargeable_weight' => 'required|integer',
            'koli.koli_width' => 'required|integer',
            'koli.koli_surcharge' => 'nullable|array',
            'koli.koli_height' => 'required|integer',
            'koli.koli_description' => 'nullable',
            'koli.koli_formula_id' => 'nullable',
            'koli.koli_volume' => 'required|integer',
            'koli.koli_weight' => 'required|integer',
            'koli.koli_id' => 'required',
            'koli.koli_custom_field' => 'nullable|array',
            'koli.koli_code' => 'required',
            'custom_field' => 'required|array',
            'custom_field.catatan_tambahan' => 'nullable',
            'current_location' => 'required|array',
            'current_location.name' => 'required',
            'current_location.code' => 'required',
            'current_location.type' => 'required'
        ];
    }
}
