<?php

namespace App\Repositories;

use App\Interfaces\PackageRepositoryInterface;
use App\Models\Transaction;

class PackageRepository implements PackageRepositoryInterface
{
    public function getAll()
    {
        return Transaction::with('connote.origin')
            ->with('connote.destination')
            ->with('connote.koli')
            ->with('connote.customField')
            ->with('connote.currentLocation')
            ->get();
    }

    public function getById(string $id)
    {
        return Transaction::with('connote.origin')
            ->with('connote.destination')
            ->with('connote.koli')
            ->with('connote.customField')
            ->with('connote.currentLocation')
            ->where('transaction_id',$id)
            ->get();
    }

    public function create(array $attributes)
    {
        return Transaction::create($attributes);
    }

    public function update(string $id, array $attributes)
    {
        $transaction = Transaction::find($id);
        $transaction->update($attributes);

        return $transaction;
    }

    public function delete(string $id)
    {
        $transaction = Transaction::find($id);

        $connote = $transaction->connote;
        $connote->origin()->delete();
        $connote->destination()->delete();
        $connote->koli()->delete();
        $connote->customField()->delete();
        $connote->currentLocation()->delete();
        $connote->delete();
        $transaction->delete();
    }
}
