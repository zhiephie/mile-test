<?php

namespace App\Repositories;

use App\Interfaces\ConnoteRepositoryInterface;
use App\Models\Connote;

class ConnoteRepository implements ConnoteRepositoryInterface
{
    public function getAll()
    {
        return Connote::all();
    }

    public function getById(string $id)
    {
        return Connote::find($id)->get();
    }

    public function create(array $attributes)
    {
        return Connote::create($attributes);
    }

    public function update(string $id, array $attributes)
    {
        //
    }

    public function delete(string $id)
    {
        return Connote::find($id)->delete();
    }
}
