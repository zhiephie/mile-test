<?php

namespace App\Services;

use App\Interfaces\ConnoteRepositoryInterface;
use App\Interfaces\PackageRepositoryInterface;

class PackageService
{
    protected $packageRepository;
    protected $connoteRepository;

    public function __construct(
        PackageRepositoryInterface $packageRepository,
        ConnoteRepositoryInterface $connoteRepository,
    )
    {
        $this->packageRepository = $packageRepository;
        $this->connoteRepository = $connoteRepository;
    }

    public function getAll()
    {
        return $this->packageRepository->getAll();
    }

    public function create(array $attributes)
    {
        $transactionData = $attributes['transaction'];
        $connoteData = $attributes['connote'];
        $originData = $attributes['origin'];
        $destinationData = $attributes['destination'];
        $koliData = $attributes['koli'];
        $customFieldData = $attributes['custom_field'];
        $currentLocationData = $attributes['current_location'];

        $transaction = $this->packageRepository->create($transactionData);
        $connote = $transaction->connote()->create($connoteData);
        $connote->origin()->create($originData);
        $connote->destination()->create($destinationData);
        $connote->koli()->create($koliData);
        $connote->customField()->create($customFieldData);
        $connote->currentLocation()->create($currentLocationData);

        return $transaction;
    }

    public function getById(string $id)
    {
       return $this->packageRepository->getById($id);
    }

    public function update(string $id, array $attributes)
    {
        $transactionData = $attributes['transaction'];
        $connoteData = $attributes['connote'];
        $originData = $attributes['origin'];
        $destinationData = $attributes['destination'];
        $koliData = $attributes['koli'];
        $customFieldData = $attributes['custom_field'];
        $currentLocationData = $attributes['current_location'];

        $transaction = $this->packageRepository->update($id, $transactionData);
        $connote = $transaction->connote;
        $connote->origin()->update($originData);
        $connote->destination()->update($destinationData);
        $connote->koli()->update($koliData);
        $connote->customField()->update($customFieldData);
        $connote->currentLocation()->update($currentLocationData);
        $connote->update($connoteData);

        return $transaction;
    }

    public function delete(string $id)
    {
        return $this->packageRepository->delete($id);
    }
}
