<?php

namespace App\Providers;

use App\Interfaces\ConnoteRepositoryInterface;
use App\Interfaces\PackageRepositoryInterface;
use App\Repositories\ConnoteRepository;
use App\Repositories\PackageRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PackageRepositoryInterface::class, PackageRepository::class);
        $this->app->bind(ConnoteRepositoryInterface::class, ConnoteRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
