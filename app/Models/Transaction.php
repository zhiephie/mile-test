<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $primaryKey = 'transaction_id';

    protected $guarded = [''];

    protected $hidden = ['_id'];

    public function connote()
    {
        return $this->hasOne(Connote::class, 'transaction_id', 'transaction_id');
    }

}
