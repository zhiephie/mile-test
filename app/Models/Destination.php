<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Destination extends Model
{

    use HasFactory;

    protected $collection = 'destination_data';
    protected $guarded = [''];
    protected $hidden = ['_id'];
}
