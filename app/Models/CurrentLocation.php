<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CurrentLocation extends Model
{

    use HasFactory;

    protected $collection = 'current_location';
    protected $guarded = [''];
    protected $hidden = ['_id'];
}
