<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Koli extends Model {

    use HasFactory;

    protected $collection = 'koli_data';
    protected $guarded = [''];
    protected $hidden = ['_id'];
}
