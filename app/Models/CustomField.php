<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CustomField extends Model
{

    use HasFactory;

    protected $collection = 'custom_field';
    protected $guarded = [''];
    protected $hidden = ['_id'];
}
