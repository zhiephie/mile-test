<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Origin extends Model
{

    use HasFactory;

    protected $collection = 'origin_data';
    protected $guarded = [''];
    protected $hidden = ['_id'];
}
