<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Connote extends Model {

    use HasFactory;

    protected $primaryKey = 'connote_id';

    protected $collection = 'connotes';

    protected $guarded = [''];

    protected $hidden = ['_id'];

    public function origin()
    {
        return $this->hasOne(Origin::class, 'connote_id', 'connote_id');
    }

    public function destination()
    {
        return $this->hasOne(Destination::class, 'connote_id', 'connote_id');
    }

    public function koli()
    {
        return $this->hasMany(Koli::class, 'connote_id', 'connote_id');
    }

    public function customField()
    {
        return $this->hasOne(CustomField::class, 'connote_id', 'connote_id');
    }

    public function currentLocation()
    {
        return $this->hasOne(CurrentLocation::class, 'connote_id', 'connote_id');
    }
}
