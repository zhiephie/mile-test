**Online Test**

**Install:**

1. Clone: `git clone gitlab.com/zhiephie/mile-test.git`
2. Run: `composer install`
3. Run: `./vendor/bin/sail build —no-cache`
4. Up: `./vendor/bin/sail up -d`
5. Run: `./vendor/bin/sail artisan migrate`
6. Test: `./vendor/bin/sail test`

**Architecture**

![Architecture](arc.png)

**API Endpoints:**

- Import `Mile.postman_collection.json` into Postman
